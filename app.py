#!/usr/bin/env python3
from flask import Flask, escape, request
from settings import Settings
from setup_wizard import Wizard
from core import Core
from api import Api
from monitor import Monitor
import os
import argparse

app = Flask(__name__)
settings = Settings()
core = Core(settings)
api = Api(app, settings, core)

@app.route("/")
def setup():
    if not settings.get("initial_setup_done"):
        wizard = Wizard()

        task: str = request.args.get("task", "intro")

        if task == "intro":
            return wizard.intro()
        elif task == "wifi":
            return wizard.setup_wifi(request)
        elif task == "ddns":
            return wizard.setup_ddns(request)
        elif task == "port_forwarding":
            return wizard.setup_port_forwarding(request)
        elif task == "xmpp_server":
            return wizard.setup_xmpp_server(request)
        elif task == "finish":
            return wizard.finish()
        else:
            return "No task with the name {} found!".format(task)
    else:
        monitor = Monitor()

        return monitor.monitor()

def restore():
    core.update_ejabberd_config()
    core.connect_to_wifi()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="KaidanBox management tool")
    parser.add_argument("--restore", help="Restore settings after an update and exit", action="store_true")
    args = parser.parse_args()

    if args.restore:
        restore()
    else:
        # Open setup wifi if needed
        if not settings.get("initial_setup_done") and not os.environ.get("FLASK_ENV") == "debug":
            core.create_hotspot("KaidanBox setup wifi")

        app.run(port=5000, host="10.7.7.115")
