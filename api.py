from flask import Flask, request, redirect
from settings import Settings
from core import Core
import requests
import json
from crontab import CronTab
from ruamel.yaml import YAML
import subprocess
import miniupnpc

class Api():
    def __init__(self, app: Flask, settings: Settings, core: Core):
        self.__app: Flask = app
        self.__settings: Settings = settings
        self.__core: Core = core

        # Register all endpoints
        self.__app.add_url_rule("/api", "api", self.api)
        self.__app.add_url_rule("/api/wifi", "api_wifi", self.api_wifi, methods=["POST"])
        self.__app.add_url_rule("/api/wifi_status", "api_wifi_status", self.api_wifi_status, methods=["GET"])
        self.__app.add_url_rule("/api/ddns", "api_ddns", self.api_ddns, methods=["POST"])
        self.__app.add_url_rule("/api/port_forwarding", "api_port_forwarding", self.api_port_forwarding, methods=["POST"])
        self.__app.add_url_rule("/api/xmpp_server_config", "api_xmpp_server_config", self.api_xmpp_server_config, methods=["GET"])
        self.__app.add_url_rule("/api/create_xmpp_server", "api_create_xmpp_server", self.api_create_xmpp_server, methods=["POST"])
        self.__app.add_url_rule("/api/config", "api_config", self.api_config, methods=["GET"])

        # Initialize yaml parser
        self.yaml: YAML = YAML(typ="rt")   # default, if not specfied, is 'rt' (round-trip)

        # The ejabberd config is not yaml compliant (yay)
        self.yaml.allow_duplicate_keys = True


    def api(self):
        return {
            "endpoints": ["/wifi", "/ddns", "/port_forwarding", "xmpp_server_config", "/create_xmpp_server", "/config"]
        }

    def api_wifi(self):
        print(request.form["ssid"], request.form["password"])

        # Store settings so we can restore them later
        self.__settings.set("wifi_ssid", request.form["ssid"])
        self.__settings.set("wifi_password", request.form["password"])

        # Connect
        # Shutdown configuration hotspot
        self.__core.remove_hotspot()
        self.__core.connect_to_wifi()

        return redirect("/api/wifi_status")

    """
    Provides a status message for the wifi connecting status page
    """
    def api_wifi_status(self):
        # Return status message
        connected_check_out: str = subprocess.check_output(["nmcli", "d", "wifi"]).decode()
        connecting_check_out: str = subprocess.check_output(["nmcli", "d"]).decode()

        for line in connected_check_out.split("\n"):
            if self.__settings.get("wifi_ssid") in line and "*" in line:
                return redirect("/?task=wifi&connecting=1&message=successfully connected")

        for line in connecting_check_out.split("\n"):
            if "wifi" in line and "connecting" in line:
                return redirect("/?task=wifi&connecting=1&warning=connecting ...")

        if "disconnected" in connecting_check_out:
            self.__core.create_hotspot("Kaidanbox Setup wifi")
            return redirect("/?task=wifi&connecting=1&error=connecting failed, check your SSID and password")

        return redirect("?task=wifi&connecting=1")


    """
    expected arguments in form
    - "ddns_host"
    - "host_name"
    """
    def api_ddns(self):
        # Register DDNS host
        baseurl = "https://{}".format(request.form["ddns_host"])
        print(baseurl)

        try:
            available_request = requests.get(baseurl + "/available/{}".format(request.form["host_name"]))
            try:
                res = json.loads(available_request.text)
                if res["available"]:
                    create_request = requests.get(baseurl + "/new/{}".format(request.form["host_name"])).text
                    # Add cron job

                    update_url = baseurl + "/{}".format(json.loads(create_request)["update_link"])
                    crontab = CronTab(user=True)

                    #job = crontab.job(command="curl {}".format(update_url))
                    #job.minute.every(1)
                    #crontab.write("test.cron")

                    self.__settings.set("host_name", "{}.{}".format(request.form["host_name"], "d.pboehm.de")) # TODO remove hardcoding
                    self.__settings.set("ddns_update_url", update_url)

                    return redirect("/?task=port_forwarding")
                else:
                    return redirect("/?task=ddns&error=Hostname is not available")
            except json.decoder.JSONDecodeError:
                print(available_request.text, available_request.url)
                return redirect("/?task=ddns&error=Service incompatible")
        except requests.exceptions.ConnectionError:
            return redirect("/?task=ddns&error=Service not found")

    """
    expected arguments in form
    - "port_min"
    - "port_max"
    """
    def api_port_forwarding(self):
        print("Trying UPnP forwarding ...")
        upnp = miniupnpc.UPnP()
        upnp.discoverdelay = 10
        upnp.discover()
        upnp.selectigd()

        for port in range(int(request.form["port_min"]), int(request.form["port_max"])):
            # addportmapping(external-port, protocol, internal-host, internal-port, description, remote-host)
            upnp.addportmapping(port, 'TCP', upnp.lanaddr, port, 'testing', '')

        # Test if ports are actually reachable
        try:
            cmd: list = ["nc", "-zv", self.__settings.get("host_name")]
            for port in range(int(request.form["port_min"]), int(request.form["port_max"])):
                print("Running", cmd + [str(port)])
                subprocess.check_call(cmd + [str(port)])

            return redirect("/?task=xmpp_server")
        except subprocess.CalledProcessError:
            return redirect("/?task=port_forwarding&error=Not all required ports are open yet, try again")


    """
    Dumps the ejabberd config in a format readable by java script (json)
    """
    def api_xmpp_server_config(self):
        return self.__core.xmpp_server_config()

    """
    Sets, collects and writes the neccessary configurations for the ejabberd config file

    Should not be called again if the custom config should be preserved, unless the
    custom config is provided as xmpp_config

    expected arguments in form:
     - xmpp_config (optional)
     - admin_password
    """
    def api_create_xmpp_server(self):
        try: # If we received user settings to apply, build based on them
            if not request.form["xmpp_config"] == "":
                new_ejabberd_config: dict = json.loads(request.form["xmpp_config"])
            else:
                new_ejabberd_config: dict = {}
        except KeyError: # Otherwise create an empty dictionary, we'll fill it later
            new_ejabberd_config: dict = {}

        # Apply ddns settings
        if self.__settings.get("host_name"):
            # Make sure the hosts key exists
            if not "hosts" in new_ejabberd_config:
                new_ejabberd_config["hosts"]: list = []

            # Add ddns host if not already included
            if not self.__settings.get("host_name") in new_ejabberd_config["hosts"]:
                new_ejabberd_config["hosts"].append(self.__settings.get("host_name"))

        # Store custom settings
        self.__settings.set("ejabberd_config", new_ejabberd_config)

        # Refresh ejabberd config from our settings
        try:
            self.__core.update_ejabberd_config()
        except:
            return redirect("/?task=xmpp_server&error=Error applying your custom configuration")

        # Register admin user
        try:
            ejabberdctl_out: str = subprocess.check_output(["ejabberdctl", "register", "admin", "localhost", request.form["admin_password"]]).decode()
            if "successfully registered" in ejabberdctl_out:
                return redirect("/?task=finish")
            else:
                return redirect("/?task=xmpp_server&error=Failed to register admin user")
        except:
            return redirect("/?task=xmpp_server&error=Error running ejabberdctl")

    """
    exposes the raw settings object to the web ui.
    Only use in case wrapping doesn't make sense, please prefer to use the
    specialized backend functions.

    expected form arguments:
     - get=key OR set=key
     - value=value (optional)
    """
    def api_config(self):
        value: str = request.args.get("value", "")

        # Fix boleans from forms TODO find proper way to submit bools
        if value == "on":
            value = True
        elif value == "off":
            value = False

        if (request.args.get("set")):
            self.__settings.set(request.args.get("set"), value)
            return redirect("/")

        if (request.args.get("get")):
            return self.__settings.get(request.args.get("get"))