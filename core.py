from ruamel.yaml import YAML
from settings import Settings
import subprocess


class Core():
    def __init__(self, settings: Settings):
        self.ejabberd_config_file="ejabberd.yml"
        self.__settings: Settings = settings

        # Initialize yaml parser
        self.yaml: YAML = YAML(typ="rt")   # default, if not specfied, is 'rt' (round-trip)

        # The ejabberd config is not yaml compliant (yay)
        self.yaml.allow_duplicate_keys = True


    """
    Opens a wifi hotspot for setup purposes
    """
    def create_hotspot(self, name):
        print("Creating hotspot")
        subprocess.call(["nmcli", "con", "add", "type", "wifi", "ifname", "wlan0", "con-name", "Hostspot", "autoconnect", "yes", "ssid", name])
        subprocess.call(["nmcli", "con", "modify", "Hostspot", "802-11-wireless.mode", "ap", "802-11-wireless.band", "bg", "ipv4.method", "shared"])
        subprocess.call(["nmcli", "con", "modify", "Hostspot", "wifi-sec.key-mgmt", "wpa-psk"])
        subprocess.call(["nmcli", "con", "modify", "Hostspot", "wifi-sec.psk", "kaidan123"])
        subprocess.call(["nmcli", "con", "up", "Hostspot"])

    """
    Removes wifi hotspot
    """
    def remove_hotspot(self):
        subprocess.call(["nmcli", "con", "down", "Hostspot"])


    """
    Dumps the ejabberd config as dict
    """
    def xmpp_server_config(self):
        ejabberd_config_file = open(self.ejabberd_config_file, "r")
        ejabberd_config: dict = self.yaml.load(ejabberd_config_file)
        ejabberd_config_file.close()

        return ejabberd_config


    """
    Private method used to update the ejabberd config from our custom settings
    """
    def update_ejabberd_config(self):
        ejabberd_config: dict = self.xmpp_server_config()

        print("Refreshing ejabberd config")

        # Restore all non default settings on top of the ejabberd.yml
        if self.__settings.get("ejabberd_config") != {}:
            ejabberd_config.update(self.__settings.get("ejabberd_config"))

        # Save new ejabberd configuration
        ejabberd_config_file = open(self.ejabberd_config_file, "w")
        self.yaml.dump(ejabberd_config, ejabberd_config_file)
        ejabberd_config_file.close()

    """
    Connect to a wifi connections stored in the store
    """
    def connect_to_wifi(self):
        subprocess.Popen(["nmcli", "d", "wifi", "c",
            self.__settings.get("wifi_ssid"),
            "password", self.__settings.get("wifi_password")])
