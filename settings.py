import json

class Settings():
    def __init__(self, config_file_path="kaidanbox.json"):
        self.__config_file_path: str = config_file_path

        # Create file in case missing
        try:
            # Try to open config file read write
            open(config_file_path, "r+").close()
        except FileNotFoundError:
            # Create file if not found
            config_file = open(config_file_path, "a")
            config_file.write(json.dumps({
                    "initial_setup_done": False
                })
            )
            config_file.close()


        # read settings
        config_file = open(config_file_path, "r")
        self.__settings: dict = json.load(config_file)
        config_file.close()

    def __write_to_disk(self):
        config_file = open(self.__config_file_path, "w")
        config_file.write(json.dumps(self.__settings, indent=4))
        config_file.close()

    def set(self, key: str, value):
        self.__settings[key] = value
        self.__write_to_disk()

    def get(self, key: str):
        try:
            return self.__settings[key]
        except KeyError:
            return False