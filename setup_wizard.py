from flask import render_template
import subprocess

class Wizard():
    def __init__(self): pass

    def intro(self):
        return render_template("intro.html")

    def setup_wifi(self, request):
        if request.args.get("connecting"):
            return render_template("wifi_connecting.html",
                error=request.args.get("error"),
                warning=request.args.get("warning"),
                message=request.args.get("message"))
        else:
            return render_template("wifi.html")

    def setup_ddns(self, request):
        return render_template("ddns.html", error=request.args.get("error"))

    def setup_xmpp_server(self, request):
        return render_template("xmpp.html", error=request.args.get("error"))

    def setup_port_forwarding(self, request):
        return render_template("port_forwarding.html", error=request.args.get("error"))

    def finish(self):
        return render_template("finish.html")